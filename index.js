//an array in programming is simple a list of data
let studentNum = ["2020-1923", "2020-1924", "2020-1925",
        "2020-1926", "2020-197"];
    
//Array
//are used to store multiple related values in a single
//variable.
//Same data type, same variable type is the best practice
//they're declared using square bracket [] AKA, 
//array literals
// syntax -> let/const arrayName = [valueA, valueB];

//common example
let grades = [91, 92, 93];
let computerBrands = ["Apple", "Asus"];

//not a good practice
let mixedArr = [12, "Asus", null, undefined, {}];

console.log(grades);
console.log(computerBrands);
console.log(mixedArr);

// Alternative way to write arrays

let myTasks = [
    "drink html",
    "eat js",
    "inhale css",
    "bake sass"
    ];

//creating an array with values from variables
let  city1 = "tokyo";
let  city2 = "Manila";
let  city3 = "Jakarta";

let cities = [city1, city2, city3];
console.log(cities);

// length property
// arrayName.length

console.log(cities.length);

let blankArr = [];

//length property can also be used with strings.
//some rray methods and properties can also be used
//with strings

let fullName = "Jamie";
console.log(fullName.length); //11 length

//length property can also set the total number
//of items in an array



//to delete a specific item/value in an array we can
//employ array methods

//Another example using  decrementation

console.log(cities);
cities.length--;
console.log(cities);

//we cand't do the same thing on strings.
//however,
console.log(fullName.length);

fullName.length = fullName.length - 1;
console.log(fullName.length);

fullName.length--;
console.log(fullName.length);

//if you can shorten the array by setting th elength property
//you can also lengthin it by adding a number into the length
//property

let theBeatles = ["John", "Paul", "Ringo", "George"];
console.log(theBeatles);
theBeatles.length++;
console.log(theBeatles);


//readyings from array
/*
        - Accessing array elements is one of the more common tasks that we do with an array
        - This can be done through the use of array indexes
        - Each element in an array is associated with it's own index/number
        - In JavaScript, the first element is associated with the number 0 and increasing this number by 1 for every element
        - The reason an array starts with 0 is due to how the language is designed
    - Array indexes actually refer to an address/location in the device's memory and how the information is stored
        - Example array location in memory
            Array address: 0x7ffe9472bad0
            Array[0] = 0x7ffe9472bad0
            Array[1] = 0x7ffe9472bad4
            Array[2] = 0x7ffe9472bad8
        - In the example above, the first element and the array itself points to the same memory location and therefore is at 0 elements away from the location of the array itself
    - Syntax
            arrayName[index];
*/

console.log(grades[0]);

//accessing an array element that does not exist will return undefined
console.log(grades[10]);

let lakers = ["Kobe", "SHaq", "Lebron", "Magic", "k"];
console.log(lakers[1]); // shaq
// access the 4th array
console.log(lakers[3]) // magic


//assign array item in another variable
let currentLakers = lakers[2];
console.log(currentLakers);

//update array values using the item indices
console.log(lakers);

//reassign method
lakers[2] = "Gasol";
console.log(lakers);

//accessing the last element of an array
// arrayname.length - 1;

let bulls = ["MJ", "Pippen", "Rodman", "Rose", "kukoc"];
let lastElement = bulls.length - 1;
console.log(bulls[lastElement]);

console.log(bulls[bulls.length - 1]);

//add an item to array
//using indeces, you can also add item into the array

let newArr = [];
console.log(newArr[0]);

newArr[0] = "cloud";
console.log(newArr);

console.log(newArr[1]);
newArr[1] = "tiff";
console.log(newArr);

newArr[newArr.length] = "Bareer";
console.log(newArr);

//looping over an array

let nums = [5, 12, 30, 46, 40];
for (let i = 0; i < nums.length; i++){
    
    if(nums[i] % 5 === 0){
        console.log(nums[i] + " is divisible by 5");
    } else {
        console.log(nums[i] + " is not divisible by 5");
    }

}

//multi dimentional arrays
let chessBoard = [
    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
];

console.table(chessBoard);

//Accessing an element of multidimentional arra

//arrayName[column][row]
//arrayName[up to down][from left to right]
console.log(chessBoard[1][4]); // 'e2'

console.log("Pawn moves to: " + chessBoard[1][5]); //f2